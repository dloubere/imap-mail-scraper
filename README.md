# IMAP mail scraper

A PHP library for scraping mail messages via IMAP

## Installation

```bash
composer require dloubere/imap-mail-scraper
```

## Examples

[example](https://gitlab.com/dloubere/imap-mail-scraper/-/blob/master/examples/example.php)
